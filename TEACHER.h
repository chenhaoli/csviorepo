#pragma once
#include "include.h"

class TEACHER
{
private:
	static int g_teacherId;
	int _id;
	string _name;
	int _age;
	string _sex;
	string _subject;

public:
	TEACHER();
	TEACHER(TEACHER* teacher_);
	TEACHER(const string newName, const int newAge, const string newSex, const string newSubject);

	void setId(const int newId);
	void setName(const string newName);
	void setAge(const int newAge);
	void setSex(const string newSex);
	void setSubject(const string newSubject);

	int getId() const;
	string getName() const;
	int getAge() const;
	string getSex() const;
	string getSubject() const;
	void printInfo();
	string strReplace(string str);
};

