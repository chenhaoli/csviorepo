#pragma once
#include "ClassParser.h"
#include "TeacherParser.h"
#include "StudentParser.h"

class CsvIO
{
private:
	map<string, Parser*> parserMap;

public:
	CsvIO();

	void writeCsv(const string flieName);
	void readCsv(const string flieName);

	vector<CLASS*> getClasses();
	vector<TEACHER*> getTeachers();
	vector<STUDENT*> getStudent();

	void printInfo();
	void cleanUp();
};

