#pragma once
#include "STUDENT.h"
#include "Parser.h"

class StudentParser : public Parser
{
public:
	vector<string> headers = { "id", "name", "age", "sex", "grade", "class" };
	vector<STUDENT*> students;

	string toCsv(void* obj);
	void fromCsv(vector<string>& headers, int index, char* value, void* obj);
	void* createInstance();
	void deleteInstance(void* obj);
	void addInstance(void* obj);
	vector<string>& getDefaultHeaders();
	void cleanUp();
	void printInfo();
};

