#pragma once
#include "CLASS.h"
#include "Parser.h"

class ClassParser:public Parser
{
public:
	vector<string> headers = { "id", "grade", "class", "teacher's name", "student number" };
	vector<CLASS*> classes;

	string toCsv(void* obj);
	void fromCsv(vector<string>& headers, int index, char* value, void* obj);
	void* createInstance();
	void deleteInstance(void* obj);
	void addInstance(void* obj);
	vector<string>& getDefaultHeaders();
	void cleanUp();
	void printInfo();
};

