#include "ClassParser.h"

//vector<string> headers = { "id", "grade", "class", "teacher's name", "student number" };

string ClassParser::toCsv(void* obj)
{
    stringstream ss;
    CLASS* ref = (CLASS*)obj;
    ss << ref->getId() << "," << ref->getGrade() << "," << ref->getClass() << "," << ref->getTeacherName() << "," << ref->getStudentNum() << ",";
    return ss.str();
}

void ClassParser::fromCsv(vector<string>& headers, int index, char* value, void* obj)
{
	CLASS* ref = (CLASS*)obj;
	if (headers[index] == "id") { ref->setId(atoi(value)); }
	else if (headers[index] == "grade") { ref->setGrade(value); }
	else if (headers[index] == "class") { ref->setClass(atoi(value)); }
	else if (headers[index] == "teacher's name") { ref->setTeacherName(value); }
	else if (headers[index] == "student number") { ref->setStudentNum(atoi(value)); }
	else { cout << "ERROR: invalid data, unknown field '" << headers[index] << "'\n"; }
}

void ClassParser::addInstance(void* obj) 
{
	CLASS* class_ = new CLASS((CLASS*)obj);
	(this->classes).push_back(class_);
}

void* ClassParser::createInstance()
{
	return new CLASS();
}

void ClassParser::deleteInstance(void* obj)
{
	delete (CLASS*)obj;
}

void ClassParser::cleanUp()
{
	for (int i = 0; i < classes.size(); ++i) {
		delete classes[i];
	}
	cout << "ClassParser clean up" << endl;
}

void ClassParser::printInfo()
{
	for (int i = 0; i < classes.size(); ++i) {
		classes[i]->printInfo();
	}
}

vector<string>& ClassParser::getDefaultHeaders()
{
	return headers;
}
