#pragma once
#include "include.h"
class STUDENT
{
private:
	static int g_studentId;
	int _id;
	string _name;
	int _age;
	string _sex;
	string _grade;
	int _class;

public:
	STUDENT();
	STUDENT(STUDENT* student_);
	STUDENT(const string newName, const int newAge, const string newSex, const string newGrade, const int newClass);

	void setId(const int newId);
	void setName(const string newName);
	void setAge(const int newAge);
	void setSex(const string newSex);
	void setGrade(const string newGrade);
	void setClass(const int newClass);

	int getId() const;
	string getName() const;
	int getAge() const;
	string getSex() const;
	string getGrade() const;
	int getClass() const;

	void printInfo();
	string strReplace(string str);
};

