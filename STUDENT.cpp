#include "STUDENT.h"

int STUDENT::g_studentId = 0;

STUDENT::STUDENT()
{
	this->_id = g_studentId++;
	this->_name = "";
	this->_age = 0;
	this->_sex = "";
	this->_grade = "";
	this->_class = 0;
}

STUDENT::STUDENT(STUDENT* student_)
{
	this->_id = student_->_id;
	this->_name = student_->_name;
	this->_age = student_->_age;
	this->_sex = student_->_sex;
	this->_grade = student_->_grade;
	this->_class = student_->_class;
}

STUDENT::STUDENT(const string newName, const int newAge, const string newSex, const string newGrade, const int newClass)
{
	string newName_ = strReplace(newName);
	string newSex_ = strReplace(newSex);
	string newGrade_ = strReplace(newGrade);

	this->_id = g_studentId++;
	this->_name = newName_;
	this->_age = newAge;
	this->_sex = newSex_;
	this->_grade = newGrade_;
	this->_class = newClass;
}

void STUDENT::setId(const int newId)
{
	this->_id = newId;
}

void STUDENT::setName(const string newName)
{
	this->_name = newName;
}

void STUDENT::setAge(const int newAge)
{
	this->_age = newAge;
}

void STUDENT::setSex(const string newSex)
{
	this->_sex = newSex;
}

void STUDENT::setGrade(const string newGrade)
{
	this->_grade = newGrade;
}

void STUDENT::setClass(const int newClass)
{
	this->_class = newClass;
}

int STUDENT::getId() const
{
	return this->_id;
}

string STUDENT::getName() const
{
	return this->_name;
}

int STUDENT::getAge() const
{
	return this->_age;
}

string STUDENT::getSex() const
{
	return this->_sex;
}

string STUDENT::getGrade() const
{
	return this->_grade;
}

int STUDENT::getClass() const
{
	return this->_class;
}

void STUDENT::printInfo()
{

	cout << "id:" << this->_id << ",";
	cout << "name:" << this->_name << ",";
	cout << "age:" << this->_age << ",";
	cout << "sex:" << this->_sex << ",";
	cout << "grade:" << this->_grade << ",";
	cout << "class:" << this->_class << endl;
}

string STUDENT::strReplace(string str)
{
	string newStr = str;
	int pos = newStr.find(',');
	while (pos != -1) {
		newStr = newStr.replace(pos, 1, "%");
		pos = newStr.find(',');
	}
	return newStr;
}