﻿#include <iostream>
#include "CsvIO.h"

int main()
{
    string flieName = "test.csv";
    CsvIO* csvIO = new CsvIO();
    csvIO->writeCsv(flieName);
    csvIO->readCsv(flieName);
    csvIO->printInfo();
    csvIO->cleanUp();
    delete csvIO;
}


