#pragma once
#include "include.h"

class CLASS
{
private:
	static int g_classId;
	int _id;
	string _grade;
	int _class;
	string _teacherName;
	int _studentNum;

public:
	CLASS();
	CLASS(CLASS* class_);
	CLASS(const string newGrade, const int newClass, const string newTeacherName, const int newStudentNum);

	void setId(const int newId);
	void setGrade(const string newGrade);
	void setClass(const int newClass);
	void setTeacherName(const string newTeacherName);
	void setStudentNum(const int newStudentNum);

	int getId() const;
	string getGrade() const;
	int getClass() const;
	string getTeacherName() const;
	int getStudentNum() const;

	void printInfo();

	string strReplace(string str);
};

