#include "TeacherParser.h"

//vector<string> headers = { "id", "name", "age", "sex", "subject" };

string TeacherParser::toCsv(void* obj)
{
	//int _id;
	//string _name;
	//int _age;
	//string _sex;
	//string _subject;
	stringstream ss;
	TEACHER* ref = (TEACHER*)obj;
	ss << ref->getId() << "," << ref->getName() << "," << ref->getAge() << "," << ref->getSex() << "," << ref->getSubject() << ",";
	return ss.str();
}

void TeacherParser::fromCsv(vector<string>& headers, int index, char* value, void* obj)
{
	TEACHER* ref = (TEACHER*)obj;
	if (headers[index] == "id") { ref->setId(atoi(value)); }
	else if (headers[index] == "name") { ref->setName(value); }
	else if (headers[index] == "age") { ref->setAge(atoi(value)); }
	else if (headers[index] == "sex") { ref->setSex(value); }
	else if (headers[index] == "subject") { ref->setSubject(value); }
	else { cout << "ERROR: invalid data, unknown field '" << headers[index] << "'\n"; }
}

void* TeacherParser::createInstance()
{
	return new TEACHER();
}

void TeacherParser::deleteInstance(void* obj)
{
	delete (TEACHER*)obj;
}

void TeacherParser::addInstance(void* obj)
{
	TEACHER* teahcer_ = new TEACHER((TEACHER*)obj);
	(this->teachers).push_back(teahcer_);
}

vector<string>& TeacherParser::getDefaultHeaders()
{
	return headers;
}

void TeacherParser::cleanUp()
{
	for (int i = 0; i < teachers.size(); ++i) {
		delete teachers[i];
	}
	cout << "TeacherParser clean up" << endl;

}

void TeacherParser::printInfo()
{
	for (int i = 0; i < teachers.size(); ++i) {
		teachers[i]->printInfo();
	}
}
