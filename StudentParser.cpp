#include "StudentParser.h"

//vector<string> headers = { "id", "name", "age", "sex", "grade", "class"};

string StudentParser::toCsv(void* obj)
{
	//int _id;
	//string _name;
	//int _age;
	//string _sex;
	//string _grade;
	//int _class;
	stringstream ss;
	STUDENT* ref = (STUDENT*)obj;
	ss << ref->getId() << "," << ref->getName() << "," << ref->getAge() << "," << ref->getSex() << "," << ref->getGrade() << "," << ref->getClass() << ",";
	return ss.str();
}

void StudentParser::fromCsv(vector<string>& headers, int index, char* value, void* obj)
{
	STUDENT* ref = (STUDENT*)obj;
	if (headers[index] == "id") { ref->setId(atoi(value)); }
	else if (headers[index] == "name") { ref->setName(value); }
	else if (headers[index] == "age") { ref->setAge(atoi(value)); }
	else if (headers[index] == "sex") { ref->setSex(value); }
	else if (headers[index] == "grade") { ref->setGrade(value); }
	else if (headers[index] == "class") { ref->setClass(atoi(value)); }
	else { cout << "ERROR: invalid data, unknown field '" << headers[index] << "'\n"; }
}

void* StudentParser::createInstance()
{
	return new STUDENT();
}

void StudentParser::deleteInstance(void* obj)
{
	delete (STUDENT*)obj;
}

void StudentParser::addInstance(void* obj)
{
	STUDENT* student_ = new STUDENT((STUDENT*)obj);
	this->students.push_back(student_);
}

vector<string>& StudentParser::getDefaultHeaders()
{
	return headers;
}

void StudentParser::cleanUp()
{
	for (int i = 0; i < students.size(); ++i) {
		delete students[i];
	}
	cout << "StudentParser clean up" << endl;
}

void StudentParser::printInfo()
{
	for (int i = 0; i < students.size(); ++i) {
		students[i]->printInfo();
	}
}
