#include "CsvIO.h"

CsvIO::CsvIO()
{
	parserMap["class"] = new ClassParser();
	parserMap["teacher"] = new TeacherParser();
	parserMap["student"] = new StudentParser();
}

void CsvIO::writeCsv(const string flieName)
{
	Parser* parser;
	ofstream ofs(flieName, ios::out);
	ofs << "----" << "class:";
	parser = parserMap["class"];
	for (int i = 0; i < parser->getDefaultHeaders().size(); ++i) {
		ofs << parser->getDefaultHeaders()[i] << ",";
	}
	ofs << "\n";
	CLASS* class_1 = new CLASS("一年,级", 1, "张三", 40);
	ofs << parser->toCsv(class_1) << "\n";
	CLASS* class_2 = new CLASS("二年,级", 2, "李四", 40);
	ofs << parser->toCsv(class_2) << "\n";
	CLASS* class_3 = new CLASS("三年,级", 3, "王五", 40);
	ofs << parser->toCsv(class_3) << "\n";
	CLASS* class_4 = new CLASS("四年,级", 4, "陈六", 40);
	ofs << parser->toCsv(class_4) << "\n";
	delete class_1, class_2, class_3, class_4;


	ofs << "----" << "teacher:";
	parser = parserMap["teacher"];
	for (int i = 0; i < parser->getDefaultHeaders().size(); ++i) {
		ofs << parser->getDefaultHeaders()[i] << ",";
	}
	ofs << "\n";
	//const string newName, const int newAge, const string newSex, const string newSubject
	TEACHER* teacher_1 = new TEACHER("张,三", 30, "男", "数学");
	ofs << parser->toCsv(teacher_1) << "\n";
	TEACHER* teacher_2 = new TEACHER("李,四", 35, "女", "语文");
	ofs << parser->toCsv(teacher_2) << "\n";
	TEACHER* teacher_3 = new TEACHER("王,五", 40, "男", "英语");
	ofs << parser->toCsv(teacher_3) << "\n";
	TEACHER* teacher_4 = new TEACHER("陈,六", 30, "女", "体育");
	ofs << parser->toCsv(teacher_4) << "\n";
	delete teacher_1, teacher_2, teacher_3, teacher_4;

	ofs << "----" << "student:";
	parser = parserMap["student"];
	for (int i = 0; i < parser->getDefaultHeaders().size(); ++i) {
		ofs << parser->getDefaultHeaders()[i] << ",";
	}
	ofs << "\n";
	//const string newName, const int newAge, const string newSex, const string newGrade, const int newClass
	STUDENT* student_1 = new STUDENT("张三,学", 9, "男", "三年级", 1);
	ofs << parser->toCsv(student_1) << "\n";
	STUDENT* student_2 = new STUDENT("李四,学", 10, "女", "四年级", 2);
	ofs << parser->toCsv(student_2) << "\n";
	STUDENT* student_3 = new STUDENT("王五,学", 11, "男", "五年级", 3);
	ofs << parser->toCsv(student_3) << "\n";
	STUDENT* student_4 = new STUDENT("陈六,学", 12, "女", "六年级", 4);
	ofs << parser->toCsv(student_4) << "\n";
	delete student_1, student_2, student_3, student_4;

	ofs.close();
	cout << "Write Csv File Success!" << endl;
}

void CsvIO::readCsv(const string flieName)
{
	ifstream ifs(flieName, ios::in);
	string line;
	string tableName = "";
	void* obj = nullptr;
	Parser* currentParser = nullptr;
	while (getline(ifs, line)) {
		stringstream ss(line);
		string str;
		if (line.substr(0, 4) == "----") {
			/*if (obj != nullptr && currentParser != nullptr) {
				currentParser->deleteInstance(obj);
			}*/
			int len = line.find(':');
			tableName = line.substr(4, len - 4);
			currentParser = parserMap[tableName];
			obj = currentParser->createInstance();
		}
		else {
			
			for (int i = 0; i < currentParser->getDefaultHeaders().size(); ++i) {
				getline(ss, str, ',');
				int pos = str.find('%');
				while (pos != -1) {
					str = str.replace(pos, 1, ",");
					pos = str.find('%');
				}
				currentParser->fromCsv(currentParser->getDefaultHeaders(), i, (char*)str.data(), obj);
			}
			currentParser->addInstance(obj);
		}
	}
	//currentParser->deleteInstance(obj);
}

vector<CLASS*> CsvIO::getClasses()
{
	return ((ClassParser*)parserMap["class"])->classes;
}
vector<TEACHER*> CsvIO::getTeachers()
{
	return ((TeacherParser*)parserMap["class"])->teachers;
	//return TeacherParser::teachers;
}
vector<STUDENT*> CsvIO::getStudent()
{
	return ((StudentParser*)parserMap["class"])->students;
	//return StudentParser::students;
}

void CsvIO::printInfo()
{
	for (map<string, Parser*>::iterator itr = parserMap.begin(); itr != parserMap.end(); ++itr) {
		(itr->second)->printInfo();
	}
	//for (int i = 0; i < ((ClassParser*)parserMap["class"])->classes.size(); ++ i) {
	//	((ClassParser*)parserMap["class"])->classes[i]->printInfo();
	//}
	//for (int i = 0; i < ((TeacherParser*)parserMap["teacher"])->teachers.size(); ++ i) {
	//	((TeacherParser*)parserMap["teacher"])->teachers[i]->printInfo();
	//}
	//for (int i = 0; i < ((StudentParser*)parserMap["student"])->students.size(); ++ i) {
	//	((StudentParser*)parserMap["student"])->students[i]->printInfo();
	//}
}

void CsvIO::cleanUp()
{
	for (map<string, Parser*>::iterator itr = parserMap.begin(); itr != parserMap.end(); ++itr) {
		(itr->second)->cleanUp();
	}
	cout << "csv IO clean up" << endl;
}
