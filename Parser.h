#pragma once
#include "include.h"

class Parser
{
public:
	virtual string toCsv(void* obj) = 0;
	virtual void fromCsv(vector<string>& headers, int index, char* value, void* obj) = 0;
	virtual void* createInstance() = 0;
	virtual void deleteInstance(void* obj) = 0;
	virtual void addInstance(void* obj) = 0;
	virtual vector<string>& getDefaultHeaders() = 0;
	virtual void cleanUp() = 0;
	virtual void printInfo() = 0;
};

