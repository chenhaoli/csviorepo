#include "TEACHER.h"

int TEACHER::g_teacherId = 0;

TEACHER::TEACHER()
{
	this->_id = g_teacherId++;
	this->_name = "";
	this->_age = 0;
	this->_sex = "";
	this->_subject = "";
}

TEACHER::TEACHER(TEACHER* teacher_)
{
	this->_id = teacher_->_id;
	this->_name = teacher_->_name;
	this->_age = teacher_->_age;
	this->_sex = teacher_->_sex;
	this->_subject = teacher_->_subject;
}

TEACHER::TEACHER(const string newName, const int newAge, const string newSex, const string newSubject)
{
	string newName_ = strReplace(newName);
	string newSex_ = strReplace(newSex);
	string newSubject_ = strReplace(newSubject);

	this->_id = g_teacherId++;
	this->_name = newName_;
	this->_age = newAge;
	this->_sex = newSex_;
	this->_subject = newSubject_;
}

void TEACHER::setId(const int newId)
{
	this->_id = newId;
}


void TEACHER::setName(const string newName)
{
	this->_name = newName;
}

void TEACHER::setAge(const int newAge)
{
	this->_age = newAge;
}

void TEACHER::setSex(const string newSex)
{
	this->_sex = newSex;
}

void TEACHER::setSubject(const string newSubject)
{
	this->_subject = newSubject;
}

int TEACHER::getId() const
{
	return this->_id;
}

string TEACHER::getName() const
{
	return this->_name;
}

int TEACHER::getAge() const
{
	return this->_age;
}

string TEACHER::getSex() const
{
	return this->_sex;
}

string TEACHER::getSubject() const
{
	return this->_subject;
}

void TEACHER::printInfo()
{
	cout << "id:" << this->_id << ",";
	cout << "name:" << this->_name << ",";
	cout << "age:" << this->_age << ",";
	cout << "sex:" << this->_sex << ",";
	cout << "subject:" << this->_subject << endl;
}
string TEACHER::strReplace(string str)
{
	string newStr = str;
	int pos = newStr.find(',');
	while (pos != -1) {
		newStr = newStr.replace(pos, 1, "%");
		pos = newStr.find(',');
	}
	return newStr;
}