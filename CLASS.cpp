#include "CLASS.h"

int CLASS::g_classId = 0;

CLASS::CLASS(const string newGrade, const int newClass, const string newTeacherName, const int newStudentNum)
{
	string newGrade_ = strReplace(newGrade);
	string newTeacherName_ = strReplace(newTeacherName);

	this->_id = g_classId++;
	this->_grade = newGrade_;
	this->_class = newClass;
	this->_teacherName = newTeacherName_;
	this->_studentNum = newStudentNum;
}

CLASS::CLASS()
{
	this->_id = g_classId++;
	this->_grade = "";
	this->_class = 0;
	this->_teacherName = "";
	this->_studentNum = 0;
}

void CLASS::setId(const int newId)
{
	this->_id = newId;
}

CLASS::CLASS(CLASS* class_)
{
	this->_id = class_->_id;
	this->_grade = class_->_grade;
	this->_class = class_->_class;
	this->_teacherName = class_->_teacherName;
	this->_studentNum = class_->_studentNum;
}

void CLASS::setGrade(const string newGrade)
{
	this->_grade = newGrade;
}

void CLASS::setClass(const int newClass)
{
	this->_class = newClass;
}

void CLASS::setTeacherName(const string newTeacherName)
{
	this->_teacherName = newTeacherName;
}

void CLASS::setStudentNum(const int newStudentNum)
{
	this->_studentNum = newStudentNum;
}

int CLASS::getId() const
{
	return this->_id;
}

string CLASS::getGrade() const
{
	return this->_grade;
}

int CLASS::getClass() const
{
	return this->_class;
}

string CLASS::getTeacherName() const
{
	return this->_teacherName;
}

int CLASS::getStudentNum() const
{
	return this->_studentNum;
}

void CLASS::printInfo()
{
	cout << "id:" << this->_id << ",";
	cout << "grade:" << this->_grade << ",";
	cout << "class:" << this->_class << ",";
	cout << "teacher's name:" << this->_teacherName << ",";
	cout << "student number:" << this->_studentNum << endl;
}

string CLASS::strReplace(string str)
{
	string newStr = str;
	int pos = newStr.find(',');
	while (pos != -1) {
		newStr = newStr.replace(pos, 1, "%");
		pos = newStr.find(',');
	}
	return newStr;
}
