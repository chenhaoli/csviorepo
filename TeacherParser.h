#pragma once
#include "TEACHER.h"
#include "Parser.h"

class TeacherParser : public Parser
{
public:
	vector<string> headers = { "id", "name", "age", "sex", "subject" };
	vector<TEACHER*> teachers;

	string toCsv(void* obj);
	void fromCsv(vector<string>& headers, int index, char* value, void* obj);
	void* createInstance();
	void deleteInstance(void* obj);
	void addInstance(void* obj);
	vector<string>& getDefaultHeaders();
	void cleanUp();
	void printInfo();
};

